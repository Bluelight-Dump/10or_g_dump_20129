## G-user 8.1.0 OPM1.171019.019 10or_G_V1_0_82 release-keys
- Manufacturer: 10or
- Platform: 
- Codename: G
- Brand: 10or
- Flavor: aosp_G-userdebug
- Release Version: 12
- Id: SP2A.220305.013.A3
- Incremental: 1648635460
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 10or/G/G:8.1.0/OPM1.171019.019/10or_G_V1_0_82:user/release-keys
- OTA version: 
- Branch: G-user-8.1.0-OPM1.171019.019-10or_G_V1_0_82-release-keys
- Repo: 10or_g_dump_20129


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
